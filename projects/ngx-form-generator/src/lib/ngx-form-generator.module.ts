import { NgModule } from '@angular/core';
import { NgxFormGeneratorComponent } from './ngx-form-generator.component';
import { TextInputComponent } from './field-components/text-input/text-input.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import { PasswordInputComponent } from './field-components/password-input/password-input.component';
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import { GroupInputComponent } from './field-components/group-input/group-input.component';
import { DynamicFieldComponent } from './field-components/dynamic-field/dynamic-field.component';
import {FieldDirective} from "./field-components/dynamic-field/field.directive";
import {CommonModule} from "@angular/common";
import { DefaultButtonComponent } from './button-component/default-button/default-button.component';
import { DynamicButtonComponent } from './button-component/dynamic-button/dynamic-button.component';
import {ButtonDirective} from "./button-component/dynamic-button/field.directive";
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [
    NgxFormGeneratorComponent,
    TextInputComponent,
    PasswordInputComponent,
    GroupInputComponent,
    DynamicFieldComponent,
    FieldDirective,
    ButtonDirective,
    DefaultButtonComponent,
    DynamicButtonComponent
  ],
  imports: [
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    CommonModule,
    MatButtonModule
  ],
  exports: [
    NgxFormGeneratorComponent
  ]
})
export class NgxFormGeneratorModule { }
