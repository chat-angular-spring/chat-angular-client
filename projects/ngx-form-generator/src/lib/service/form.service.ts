import {BaseFieldEntity} from "../entity/base-field.entity";
import {FormGroup} from "@angular/forms";
import {Injectable} from "@angular/core";

@Injectable(
  {
    providedIn: 'root'
  }
)
export class FormService {
  constructor() {
  }

  toFormGroup(fields: {
    [key: string]: BaseFieldEntity
  }) {
    const group: any = {};

    for (let key of Object.keys(fields)) {
      group[key] = fields[key].toFormControl()
    }

    return new FormGroup(group);
  }
}
