import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {FormGroup} from "@angular/forms";
import {FormService} from "./service/form.service";
import {FormInterface} from "./interface/form.interface";

@Component({
  selector: 'lib-ngx-form-generator',
  templateUrl: './ngx-form-generator.component.html',
  styles: [
  ]
})
export class NgxFormGeneratorComponent implements OnInit{
  @Input() form!: FormInterface
  @Input() buttonText?: string

  @Output() onEndFilling = new EventEmitter<any>()

  formGroup!: FormGroup

  constructor(private service: FormService) {
  }

  submit() {
    this.onEndFilling.emit(this.formGroup.value)
  }

  getKeys() {
    return Object.keys(this.form)
  }

  ngOnInit(): void {
    this.formGroup = this.service.toFormGroup(this.form)
  }
}
