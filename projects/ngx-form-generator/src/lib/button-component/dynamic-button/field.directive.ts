import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[buttonHost]',
})
export class ButtonDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
