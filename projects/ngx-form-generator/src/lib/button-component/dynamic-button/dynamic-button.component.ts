import {
  Component,
  ComponentRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  Type,
  ViewChild
} from '@angular/core';
import {ButtonComponent} from "../button.component";
import {ButtonDirective} from "./field.directive";
import {DefaultButtonComponent} from "../default-button/default-button.component";


@Component({
  selector: 'lib-dynamic-button',
  templateUrl: './dynamic-button.component.html',
  styleUrls: ['./dynamic-button.component.css']
})
export class DynamicButtonComponent implements OnInit, OnChanges{
  @Input() buttonComponent: Type<ButtonComponent> = DefaultButtonComponent
  @Input() buttonText?: string
  @Input() disabled: boolean = false
  @Input() type?: string
  @Output() onButtonClick = new EventEmitter<void>()

  @ViewChild(ButtonDirective, {static: true}) buttonHost!: ButtonDirective;

  componentRef!: ComponentRef<ButtonComponent>

  ngOnInit(): void {
    const viewContainerRef = this.buttonHost.viewContainerRef;
    viewContainerRef.clear();
    this.componentRef = viewContainerRef.createComponent<ButtonComponent>(this.buttonComponent);
    this.componentRef.instance.onButtonClick = this.onButtonClick
    this.componentRef.instance.buttonText = this.buttonText ?? "Save"
    this.componentRef.instance.disabled = this.disabled
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.componentRef){
      return
    }

    this.componentRef.instance.onButtonClick = this.onButtonClick
    this.componentRef.instance.buttonText = this.buttonText ?? "Save"
    this.componentRef.instance.disabled = this.disabled
  }
}
