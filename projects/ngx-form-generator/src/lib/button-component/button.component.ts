import {EventEmitter} from "@angular/core";

export interface ButtonComponent {
  onButtonClick: EventEmitter<void>
  buttonText: string
  disabled: boolean
  type?: string
}
