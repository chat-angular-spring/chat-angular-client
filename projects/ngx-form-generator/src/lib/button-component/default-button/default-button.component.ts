import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ButtonComponent} from "../button.component";

@Component({
  selector: 'lib-default-button',
  templateUrl: './default-button.component.html',
  styleUrls: ['./default-button.component.css']
})
export class DefaultButtonComponent implements ButtonComponent {

  @Output() onButtonClick!: EventEmitter<void>;
  @Input() buttonText!: string;
  @Input() disabled: boolean = false;
  @Input() type?: string;

}
