import {BaseFieldEntity} from "../entity/base-field.entity";

export interface FormInterface {
  [key: string]: BaseFieldEntity
}
