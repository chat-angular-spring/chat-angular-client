import {FormGroup} from "@angular/forms";

export interface FieldComponent {
  name: string
  key: string
  formGroup: FormGroup
  value?: string
  error?: string
  data?: any
}
