import {Component, Input} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {FieldComponent} from "../field.component";
import {BaseFieldEntity} from "../../entity/base-field.entity";

@Component({
  selector: 'lib-group-input',
  templateUrl: './group-input.component.html',
  styleUrls: ['./group-input.component.css']
})
export class GroupInputComponent implements FieldComponent {
  @Input() name !: string
  @Input() key!: string
  @Input() formGroup!: FormGroup
  @Input() value?: string
  @Input() error?: string
  @Input() data!: BaseFieldEntity[]

  getSubFormGroup(): FormGroup{
    return this.formGroup.controls[this.key] as FormGroup
  }
}
