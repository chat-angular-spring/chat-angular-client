import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[fieldHost]',
})
export class FieldDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
