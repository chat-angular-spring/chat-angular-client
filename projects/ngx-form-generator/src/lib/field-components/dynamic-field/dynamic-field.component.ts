import {Component, ComponentRef, Input, OnInit, ViewChild} from '@angular/core';
import {FieldDirective} from "./field.directive";
import {FieldComponent} from "../field.component";
import {BaseFieldEntity} from "../../entity/base-field.entity";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'lib-dynamic-field',
  templateUrl: './dynamic-field.component.html',
  styleUrls: ['./dynamic-field.component.css']
})
export class DynamicFieldComponent implements OnInit{
  @Input() key!: string
  @Input() field!: BaseFieldEntity
  @Input() formGroup!: FormGroup

  @ViewChild(FieldDirective, {static: true}) fieldHost!: FieldDirective;

  componentRef?: ComponentRef<FieldComponent>

  ngOnInit(): void {
    const viewContainerRef = this.fieldHost.viewContainerRef;
    viewContainerRef.clear();
    this.componentRef = viewContainerRef.createComponent<FieldComponent>(this.field.component);
    this.componentRef.instance.value = this.field.value
    this.componentRef.instance.key = this.key
    this.componentRef.instance.name = this.field.name
    this.componentRef.instance.data = this.field.data
    this.componentRef.instance.formGroup = this.formGroup
    this.componentRef.instance.error = this.genErrorMessage()

    this.formGroup.controls[this.key].valueChanges.subscribe(() => {
      if (!this.componentRef){
        return
      }

      this.componentRef.instance.error = this.genErrorMessage()
    })
  }

  genErrorMessage(): string | undefined {
    let errors = this.formGroup.get(this.key)?.errors

    if (!errors){
      return undefined
    }

    let key = Object.keys(errors)[0]
    let message = `${key}: ${JSON.stringify(errors[key])}`

    if (!this.field.errorsMassages){
      return message
    }

    let messageFromMap = this.field.errorsMassages[key]

    if (!messageFromMap){
      return message
    }

    return messageFromMap
  }
}
