import {Component, Input} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {FieldComponent} from "../field.component";

@Component({
  selector: 'lib-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.css']
})
export class TextInputComponent implements FieldComponent {
  @Input() name !: string
  @Input() key!: string
  @Input() formGroup!: FormGroup
  @Input() value?: string
  @Input() error?: string
}
