import {Component, Input} from '@angular/core';
import {FieldComponent} from "../field.component";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'lib-password-input',
  templateUrl: './password-input.component.html',
  styleUrls: ['./password-input.component.css']
})
export class PasswordInputComponent implements FieldComponent {
  @Input() name !: string
  @Input() key!: string
  @Input() formGroup!: FormGroup
  @Input() value?: string
  @Input() error?: string

  hide = true
}
