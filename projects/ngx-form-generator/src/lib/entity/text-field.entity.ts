import {BaseFieldEntity} from "./base-field.entity";
import {TextInputComponent} from "../field-components/text-input/text-input.component";
import {AbstractControlOptions, AsyncValidatorFn, ValidatorFn} from "@angular/forms";

export class TextFieldEntity extends BaseFieldEntity{
  constructor(data: {
    name: string
    value?: string
    validators?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null
    errorsMassages?: {[key: string]: string}
  }) {
    super({
      name: data.name,
      value: data.value,
      validators: data.validators,
      asyncValidator: data.asyncValidator,
      errorsMassages: data.errorsMassages,
      component: TextInputComponent
    });
  }
}
