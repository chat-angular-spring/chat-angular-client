import {BaseFieldEntity} from "./base-field.entity";
import {GroupInputComponent} from "../field-components/group-input/group-input.component";
import {AbstractControl, FormGroup} from "@angular/forms";

export class GroupFieldEntity extends BaseFieldEntity{
  constructor(data: {
    name: string
    fields: {
      [key: string]: BaseFieldEntity
    }
  }) {
    super({
      name: data.name,
      data: data.fields,
      component: GroupInputComponent
    });
  }

  override toFormControl(): AbstractControl {
    const group: any = {};

    for (let key of Object.keys(super.data)){
      group[key] = super.data[key].toFormControl()
    }

    return new FormGroup(group);
  }
}
