import {Type} from "@angular/core";
import {FieldComponent} from "../field-components/field.component";
import {
  AbstractControl,
  AbstractControlOptions,
  AsyncValidatorFn,
  FormControl,
  ValidatorFn,
} from "@angular/forms";

export class BaseFieldEntity {
  name: string
  value?: string
  data?: any
  component: Type<FieldComponent>
  validators?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null
  asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null
  errorsMassages?: {
    [key: string]: string
  }

  constructor(data: {
    name: string
    value?: string
    data?: any
    validators?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null
    errorsMassages?: {[key: string]: string}
    component: Type<FieldComponent>
  }) {
    this.name = data.name
    this.value = data.value
    this.data = data.data
    this.validators = data.validators
    this.asyncValidator = data.asyncValidator
    this.errorsMassages = data.errorsMassages
    this.component = data.component
  }

  toFormControl(): AbstractControl{
    return new FormControl(this.value ?? '', this.validators, this.asyncValidator)
  }
}
