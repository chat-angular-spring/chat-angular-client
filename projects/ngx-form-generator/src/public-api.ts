/*
 * Public API Surface of ngx-form-generator
 */

export * from './lib/ngx-form-generator.component';
export * from './lib/ngx-form-generator.module';
export * from './lib/entity/base-field.entity';
export * from './lib/entity/group-field.entity';
export * from './lib/entity/text-field.entity';
export * from './lib/entity/password-field.entity';
export * from './lib/interface/form.interface'
