import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function replyPasswordValidators(key: string): ValidatorFn {
  return ((control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null
    }

    let formControls = control.parent?.controls

    if (!formControls) {
      return null
    }

    let sourceControl = (formControls as {[key: string]: AbstractControl})[key]

    if (!sourceControl) {
      return null
    }

    if (sourceControl.value == control.value) {
      return null
    }

    return {notSame: true}
  })
}
