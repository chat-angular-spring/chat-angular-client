import {Component} from '@angular/core';
import {FormInterface} from "../../../../projects/ngx-form-generator/src/lib/interface/form.interface";
import {TextFieldEntity} from "../../../../projects/ngx-form-generator/src/lib/entity/text-field.entity";
import {Validators} from "@angular/forms";
import {PasswordFieldEntity} from "../../../../projects/ngx-form-generator/src/lib/entity/password-field.entity";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AuthService} from "../service/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  form: FormInterface = {
    email: new TextFieldEntity({
        name: "Email",
        validators: [Validators.required, Validators.email],
        errorsMassages: {
          email: "Invalid email",
          required: "Email required"
        }
      }
    ),
    password: new PasswordFieldEntity({
      name: "Password",
      validators: [Validators.required],
      errorsMassages: {
        required: "Password required"
      }
    })
  }

  constructor(private service: AuthService, private router: Router, private snackBar: MatSnackBar) {
  }

  login($event: any) {
    this.service.login($event.email, $event.password).subscribe({
      error: (err) => {
        console.log(err)
        this.snackBar.open(err, "Ok")
      },
      complete: () => {
        this.router.navigate(["/"])
      }
    })
  }

}
