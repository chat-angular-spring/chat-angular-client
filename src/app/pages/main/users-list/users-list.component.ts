import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ChatService, User} from "../../service/chat.service";


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  chats: User[] = []
  searchString?: string

  @Output() selectUserId = new EventEmitter<number>();
  @ViewChild('searchField') searchField!: ElementRef;

  constructor(private service: ChatService) { }

  ngOnInit(): void {
    this.findChats()
    setInterval(() => {
      if (this.searchString && this.searchString != "")
        return

      this.findChats()
    }, 500);
  }

  search() {
    this.searchString = this.searchField.nativeElement.value

    if (!this.searchString || this.searchString == ""){
      this.findChats()
      return
    }

    this.service.findChats(this.searchString as string).subscribe({
      next: data => {
        this.chats = data
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  selectionChange($event: any) {
    this.selectUserId.emit($event.option.value)
  }

  findChats() {
    this.service.findActiveChats().subscribe({
      next: data => {
        this.chats = data
      },
      error: (err) => {
        console.log(err)
      }
    })
  }
}
