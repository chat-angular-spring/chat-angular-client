import {Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {ChatService, Message} from "../../service/chat.service";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnChanges{

  @Input() id?: number
  @ViewChild('textField') textField!: ElementRef;

  messages: Message[] = []

  constructor(private service: ChatService) {
  }

  ngOnInit(): void {
    this.findMessages()
  }

  ngOnChanges(changes: SimpleChanges): void{
    this.findMessages()
    setInterval(() => {
      this.findMessages()
    }, 500);
  }

  send() {
    if (!this.textField.nativeElement.value || this.textField.nativeElement.value == "") {
      return
    }

    this.service.sendMessage(this.id as number, this.textField.nativeElement.value).subscribe(
      {
        error: (err) => {
          console.log(err)
        },
        complete: () => {
          this.textField.nativeElement.value = ""
        }
      }
    )
  }

  findMessages() {
    if (!this.id){
      return
    }

    this.service.getAllMessages(this.id as number).subscribe(
      {
        next: data => {
          this.messages = data
        },
        error: (err) => {
          console.log(err)
        },
      }
    )
  }
}
