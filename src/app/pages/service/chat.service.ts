import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

export interface User {
  id: number
  email: string
}

export interface Message {
  isMe: boolean
  text: string
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient) {
  }

  findActiveChats() {
    return this.http.get<any>(`http://localhost:8080/api/v1/chats`)
  }

  findChats(email: string) {
    return this.http.get<any>(`http://localhost:8080/api/v1/chats`, {params: {email: email}})
  }

  sendMessage(id: number, message: string) {
    return this.http.post<any>(`http://localhost:8080/api/v1/chats/${id}`, {
      text: message
    })
  }

  getAllMessages(id: number): Observable<Message[]>{
    return this.http.get<any>(`http://localhost:8080/api/v1/chats/${id}`)
  }
}
