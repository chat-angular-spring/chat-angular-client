import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

export interface RegistrationRequest {
  email: string
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  register(request: RegistrationRequest) {
    return this.http.post<any>(`http://localhost:8080/api/v1/auth/register`, request)
  }
}
