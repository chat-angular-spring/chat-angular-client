import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Observable, tap} from "rxjs";

export const ACCESS_TOKEN_KEY: string = "access_token"

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {
  }

  login(email: string, password: string): Observable<any> {
    return this.http.post<any>(`http://localhost:8080/api/v1/auth/login`, {
      email: email,
      password: password
    }).pipe(tap(data => {
        localStorage.setItem(ACCESS_TOKEN_KEY, data.token)
      })
    )
  }

  isAuthenticated(): boolean {
    let token = this.findToken()
    return token != null
  }

  findToken(): string | undefined{
    let token = localStorage.getItem(ACCESS_TOKEN_KEY)
    if (token != null && !this.jwtHelper.isTokenExpired(token)){
      return token
    }
    localStorage.removeItem(ACCESS_TOKEN_KEY)
    return undefined
  }
}
