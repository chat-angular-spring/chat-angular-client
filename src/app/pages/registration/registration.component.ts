import {Component} from '@angular/core';
import {TextFieldEntity} from "../../../../projects/ngx-form-generator/src/lib/entity/text-field.entity";
import {FormInterface} from "../../../../projects/ngx-form-generator/src/lib/interface/form.interface";
import {Validators} from "@angular/forms";
import {PasswordFieldEntity} from "../../../../projects/ngx-form-generator/src/lib/entity/password-field.entity";
import {replyPasswordValidators} from "../../validators/reply-password.validators";
import {UserService} from "../service/user.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  form: FormInterface = {
    email: new TextFieldEntity({
      name: "Email",
      validators: [Validators.required, Validators.email],
      errorsMassages: {
        email: "Invalid email",
        required: "Email required"
      }
    }
    ),
    password: new PasswordFieldEntity({
      name: "Password",
      validators: [Validators.required],
      errorsMassages: {
        required: "Password required"
      }
    }),
    replyPassword: new PasswordFieldEntity({
      name: "Reply password",
      validators: [Validators.required, replyPasswordValidators('password')],
      errorsMassages: {
        notSame: "Passwords not same",
        required: "Reply password required"
      }
    })
  }

  constructor(private service: UserService, private router: Router, private snackBar: MatSnackBar) {
  }

  register($event: any) {
    this.service.register({email: $event.email, password: $event.password}).subscribe({
      error: (err) => {
        console.log(err.error.non_field_errors[0])
        this.snackBar.open(err.error.non_field_errors[0], "Ok")
      },
      complete: () => {
        this.router.navigate(["login"])
      }
    })
  }
}
